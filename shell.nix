{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = with pkgs.buildPackages; [ haskell-language-server stack ghc haskellPackages.hakyll zlib ];
}